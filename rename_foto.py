#!/usr/bin/env python

"""rename.py: This application was created for renaming photos."""

__author__      = "Pavel Hosek"
__copyright__   = "Copyright 2014, Pavel Hosek"
__license__ = "Public Domain"
__version__ = "1.0"

from gi.repository import Gtk, GObject
import os
import datetime
import exifread

class DialogWindow(Gtk.Window):
	
	def __init__(self, typeDialog, errorList=None):
		Gtk.Window.__init__(self, title="Dialog Window")
		self.set_border_width(20)
		#self.set_size_request(200,150)
		self.set_resizable(False)

		vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
		self.add(vbox)
		self.label = Gtk.Label()
		if(typeDialog == "directory"):
			self.label.set_markup("<b><big>The Directory does not exist!</big></b>")
			self.label.set_line_wrap(True)
			self.lbSecondInformation = Gtk.Label("Change the directory path.")
		elif(typeDialog == "file"):
			text = ""
			for (key, val) in errorList.items():
				text += str(key + " - " + val + "\n")
			self.label.set_markup("<b><big>Not renamed files!</big></b>")
			self.label.set_line_wrap(True)
			self.lbSecondInformation = Gtk.Label(text)
		elif(typeDialog == "empty"):
			self.label.set_markup("<b><big>The Directory is empty!</big></b>")
			self.label.set_line_wrap(True)
			self.lbSecondInformation = Gtk.Label(text)
		vbox.pack_start(self.label, True, True, 0)
		vbox.pack_start(self.lbSecondInformation, True, True, 0)
		self.show()

class MyWindow(Gtk.Window):

	def __init__(self):
		Gtk.Window.__init__(self, title="")
		self.set_border_width(10)
		self.set_resizable(False)

		hb = Gtk.HeaderBar()
		hb.props.show_close_button = True
		hb.props.decoration_layout_set = False
		hb.props.title = "Rename photo"
		self.set_titlebar(hb)

		## version 1
#		vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
#		hbox1 = Gtk.Box(spacing=10)
#		hbox2 = Gtk.Box(spacing=10)
#		hbox3 = Gtk.Box(spacing=10)
#		hbox4 = Gtk.Box(spacing=10)
#		hbox5 = Gtk.Box(spacing=10)
#		hbox6 = Gtk.Box(spacing=10)
#
#		self.add(vbox1)

		## version 2
		vbox1 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
		vboxLeft = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
		vboxRight = Gtk.Box(orientation = Gtk.Orientation.VERTICAL, spacing=10)
		hbox1 = Gtk.Box(spacing=10)
		hboxButton = Gtk.Box(spacing=10)

		self.add(vbox1)

		self.lbDirectory = Gtk.Label("Directory:")
		self.lbSuffixFileName = Gtk.Label("Suffix file name:")
		self.lbPrefixFileName = Gtk.Label("Prefix file name:")
		self.lbRename = Gtk.Label("New file name prefix:")
		self.lbRenameBy = Gtk.Label("Rename by:")
		self.lbDateTimeFormat = Gtk.Label("Date and time format:")
		self.lbEmpty = Gtk.Label("  ")

		self.entryDirectory = Gtk.Entry()
		self.entryDirectory.set_text("/mnt/data/Soukrome/Obrazky a fotky")
		self.entryDirectory.set_size_request(500,20)
		self.entryPrefixFileName = Gtk.Entry()
		self.entryPrefixFileName.set_text("DSC")
		self.entrySuffixFileName = Gtk.Entry()
		self.entrySuffixFileName.set_text("jpg")
		self.entryRename = Gtk.Entry()
		self.entryRename.set_text("Write new name")
		self.entryDateTimeFormat = Gtk.Entry()
		self.entryDateTimeFormat.set_text("%Y-%m-%d %H.%M.%S")

		self.radioButton1 = Gtk.RadioButton.new_with_label_from_widget(None, "File - Modified time")
		self.radioButton1.set_size_request(245,20)
		self.radioButton1.connect("toggled", self.on_button_toggled, "1")
		self.radioButton2 = Gtk.RadioButton.new_from_widget(self.radioButton1)
		self.radioButton2.set_label("EXIF - DateTimeOriginal")
		self.radioButton2.set_size_request(245,20)
		self.radioButton2.connect("toggled", self.on_button_toggled, "2")

		self.button = Gtk.Button(label="Rename")
		self.button.set_size_request(150,20)
		self.button.connect("clicked", self.on_button_clicked)
		self.buttonExit = Gtk.Button(label="Exit  ")
		self.buttonExit.set_size_request(150,20)
		self.buttonExit.connect("clicked", self.on_close_clicked)
		self.buttonFolder = Gtk.Button(label="Select Folder")
		self.buttonFolder.set_size_request(150,20)
		self.buttonFolder.connect("clicked", self.on_folder_clicked)
		
		## version 1
#		vbox.pack_start(self.lbDirectory, True, True, 0)
#		vbox.pack_start(self.entryDirectory, True, True, 0)
#		vbox.pack_start(hbox1, True, True, 0)
#		hbox1.pack_start(self.lbPrefixFileName, True, True, 0)
#		hbox1.pack_start(self.lbSuffixFileName, True, True, 0)
#		vbox.pack_start(hbox2, True, True, 0)
#		hbox2.pack_start(self.entryPrefixFileName, True, True, 0)
#		hbox2.pack_start(self.entrySuffixFileName, True, True, 0)
#		vbox.pack_start(hbox3, True, True, 0)
#		hbox3.pack_start(self.lbRename, True, True, 0)
#		hbox3.pack_start(self.lbDateTimeFormat, True, True, 0)
#		vbox.pack_start(hbox4, True, True, 0)
#		hbox4.pack_start(self.entryRename, True, True, 0)
#		hbox4.pack_start(self.entryDateTimeFormat, True, True, 0)
#		vbox.pack_start(self.lbRenameBy, True, True, 0)
#		vbox.pack_start(hbox5,True, True, 0)
#		hbox5.pack_start(self.radioButton1, True, True, 0)
#		hbox5.pack_start(self.radioButton2, True, True, 0)
#		vbox.pack_start(hbox6, True, True, 0)
#		hbox6.pack_start(self.button, True, True, 0)
#		hbox6.pack_start(self.buttonExit, True, True, 0)

		## version 2
		vbox1.pack_start(self.lbDirectory, True, True, 0)
		vbox1.pack_start(self.entryDirectory, True, True, 0)
		vbox1.pack_start(hbox1, True, True, 0)
		hbox1.pack_start(vboxLeft, True, True, 0)
		hbox1.pack_start(vboxRight, True, True, 0)
		vboxLeft.pack_start(self.lbPrefixFileName, True, True, 0)
		vboxLeft.pack_start(self.lbSuffixFileName, True, True, 0)
		vboxLeft.pack_start(self.lbRename, True, True, 0)
		vboxLeft.pack_start(self.lbDateTimeFormat, True, True, 0)
		vboxLeft.pack_start(self.lbRenameBy, True, True, 0)
		vboxLeft.pack_start(self.lbEmpty, True, True, 0)
		vboxRight.pack_start(self.entryPrefixFileName, True, True, 0)
		vboxRight.pack_start(self.entrySuffixFileName, True, True, 0)
		vboxRight.pack_start(self.entryRename, True, True, 0)
		vboxRight.pack_start(self.entryDateTimeFormat, True, True, 0)
		vboxRight.pack_start(self.radioButton1, True, True, 0)
		vboxRight.pack_start(self.radioButton2, True, True, 0)
		vbox1.pack_start(hboxButton, True, True, 0)
		hboxButton.pack_start(self.buttonFolder, True, True, 0)
		hboxButton.pack_start(self.button, True, True, 0)
		hboxButton.pack_start(self.buttonExit, True, True, 0)

		self.selectTime = "ModifiedTime"
		self.dateTimeFormat = "%Y-%m-%d %H.%M.%S"
		self.fileWithError = {}

	def on_button_clicked(self, button):
		self.fileWithError = {}
		if(os.path.isdir(self.entryDirectory.get_text())):
			if(not os.listdir(self.entryDirectory.get_text())):
				dialog = DialogWindow("empty")
				dialog.show_all()
			else:
				os.chdir(self.entryDirectory.get_text())
				for filename in os.listdir("./"):
						if(os.path.isfile(filename) and self.control_pre_suf(filename, self.entryPrefixFileName.get_text(), self.entrySuffixFileName.get_text())):
							time = self.select_time(filename, self.selectTime)
							if(time != "error"):
								self.rename(filename, time)
							else:
								self.fileWithError[filename] = str("This file does not contain exif data.")
								print(filename + " has not been renamed.")
		else:
			dialog = DialogWindow("directory")
			dialog.show_all()
		if(len(self.fileWithError) > 0):
			dialog = DialogWindow("file", self.fileWithError)
			dialog.show_all()


	def on_close_clicked(self, buttonExit):
		print("Closing application")
		Gtk.main_quit()

	def on_button_toggled(self, button, name):
		if button.get_active() and name == "1":
			print("Select time of file.")
			self.selectTime = "ModifiedTime"
		elif button.get_active() and name == "2":
			print("Select time of exif.")
			self.selectTime = "DateTimeOriginal"

	def select_time(self, filename, typeOfTime):
		endtime = ""
		if(typeOfTime == "ModifiedTime"):
			startTime = os.path.getmtime(filename)
			startTime = datetime.datetime.fromtimestamp(startTime)
			endTime = startTime.strftime(self.entryDateTimeFormat.get_text())
		elif(typeOfTime == "DateTimeOriginal"):
			path = str(os.path.abspath("./") + "/" + filename)
			f = open(path, "rb")
			tags = exifread.process_file(f)
			if "EXIF DateTimeOriginal" in tags.keys():
				dt = datetime.datetime.strptime(str(tags["EXIF DateTimeOriginal"]), "%Y:%m:%d %H:%M:%S")
				endTime = dt.strftime(self.entryDateTimeFormat.get_text())
			else:
				endTime = "error"
		return endTime

	def control_pre_suf(self, filename, prefix, suffix):
		if(prefix.lower() == filename[:len(prefix)].lower() and suffix.lower() == filename[-len(suffix):].lower()):
			return True
		elif(prefix.lower() == filename[:len(prefix)].lower() and suffix.lower() == ""):
			return True
		else:
			return False

	def rename(self, oldName, time):
		newName = ""
		if(self.entryRename.get_text() == "Write new name"):
			newName = str("" + time + oldName[-4:]).lower()
		else:	
			newName = str(self.entryRename.get_text() + time + oldName[-4:]).lower()
		if(os.path.isfile(newName)):
			self.fileWithError[oldName] = str(newName + " exists.")
			print(oldName + " has not been renamed.")
		else:
			os.rename(oldName, newName)
			print(oldName + " was renamed.")

	def on_folder_clicked(self, widget):
		dialog = Gtk.FileChooserDialog("Please choose a folder", self, Gtk.FileChooserAction.SELECT_FOLDER, (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, "Select", Gtk.ResponseType.OK))
		dialog.set_default_size(800, 400)
		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			print("Select clicked")
			print("Folder selected: " + dialog.get_filename())
			self.entryDirectory.set_text(dialog.get_filename())
		elif response == Gtk.ResponseType.CANCEL:
			print("Cancel clicked")
		dialog.destroy()

##class FileChooserWindow(Gtk.Window):
##
##    def __init__(self):
##        Gtk.Window.__init__(self, title="FileChooser Example")
##
##        box = Gtk.Box(spacing=6)
##        self.add(box)
##
##        button1 = Gtk.Button("Choose File")
##        button1.connect("clicked", self.on_file_clicked)
##        box.add(button1)
##
##        button2 = Gtk.Button("Choose Folder")
##        button2.connect("clicked", self.on_folder_clicked)
##        box.add(button2)
##
##    def on_file_clicked(self, widget):
##        dialog = Gtk.FileChooserDialog("Please choose a file", self,
##            Gtk.FileChooserAction.OPEN,
##            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
##             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
##
##        self.add_filters(dialog)
##
##        response = dialog.run()
##        if response == Gtk.ResponseType.OK:
##            print("Open clicked")
##            print("File selected: " + dialog.get_filename())
##        elif response == Gtk.ResponseType.CANCEL:
##            print("Cancel clicked")
##
##        dialog.destroy()
##
##    def add_filters(self, dialog):
##        filter_text = Gtk.FileFilter()
##        filter_text.set_name("Text files")
##        filter_text.add_mime_type("text/plain")
##        dialog.add_filter(filter_text)
##
##        filter_py = Gtk.FileFilter()
##        filter_py.set_name("Python files")
##        filter_py.add_mime_type("text/x-python")
##        dialog.add_filter(filter_py)
##
##        filter_any = Gtk.FileFilter()
##        filter_any.set_name("Any files")
##        filter_any.add_pattern("*")
##        dialog.add_filter(filter_any)
##
##    def on_folder_clicked(self, widget):
##        dialog = Gtk.FileChooserDialog("Please choose a folder", self,
##            Gtk.FileChooserAction.SELECT_FOLDER,
##            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
##             "Select", Gtk.ResponseType.OK))
##        dialog.set_default_size(800, 400)
##
##        response = dialog.run()
##        if response == Gtk.ResponseType.OK:
##            print("Select clicked")
##            print("Folder selected: " + dialog.get_filename())
##        elif response == Gtk.ResponseType.CANCEL:
##            print("Cancel clicked")
##
##        dialog.destroy()
##
##fch = FileChooserWindow()
##fch.connect("delete-event", Gtk.main_quit)
##fch.show_all()
win = MyWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()